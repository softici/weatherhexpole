# syntax=docker/dockerfile:1

FROM ubuntu:22.04

WORKDIR /app

COPY . .

RUN apt update
RUN apt install python3 -y
RUN apt install curl -y
RUN curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
RUN python3 get-pip.py
RUN pip install requests
RUN pip install mysql-connector-python

RUN chmod -R 0777 /app

ENTRYPOINT ["./launcher.bash"]

#CMD ["bash", "laucher.bash"]


